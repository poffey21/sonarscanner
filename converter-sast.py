import json
import os.path
import uuid

"""
required:
version
vulnerabilities[{}]
    id
    identifiers[{}]
    location

scan{}
    start_time
    end_time
    analyzer{}
        "id",
        "name",
        "version",
        "vendor"
    scanner{}
        "id",
        "name",
        "version",
        "vendor"
    status
    type

"""

def lookup(severity):

    vv = {
        'info': 'Info',
        'minor': 'Medium',
        'major': 'High',
        'blocker': 'Critical',
        'critical': 'Critical',
    }

    return vv.get(severity, 'Unknown')

data_spec = {
  "version": "15.0.7",
  "vulnerabilities": [
    {
      "id": "642735a5-1425-428d-8d4ddaaf",
      "name": "Improper Control of Generation of Code ('Code Injection')",
      "description": "The application was found calling the `eval` function with a non-literal variable.",
      "severity": "Medium",
      "location": {
        "file": "converter.py",
        "start_line": 10
      },
      "identifiers": [
        {
          "type": "cwe",
          "name": "CWE-95",
          "value": "95",
          "url": "https://cwe.mitre.org/data/definitions/95.html"
        }
      ],
    },
  ],
  "scan": {
    "analyzer": {
      "id": "sonarqube",
      "name": "SonarQube A N",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/semgrep",
      "vendor": {
        "name": "SonarQube A V N"
      },
      "version": ":SKIP:"
    },
    "scanner": {
      "id": "sonarqube",
      "name": "SonarQube S N",
      "url": "https://github.com/returntocorp/semgrep",
      "vendor": {
        "name": "SonarQube S V N"
      },
      "version": ":SKIP:"
    },
    "type": "sast",
    "start_time": "2024-01-08T03:26:02",
    "end_time": "2024-01-08T03:28:13",
    "status": "success"
  }
}

def main():
    data_source = []
    if os.path.isfile('gl-code-quality-report.json'):
        with open('gl-code-quality-report.json', 'r') as f:
            data_source = json.loads(f.read())

    vulnerabilities = []

    for check in data_source:
        name = check.get('check_name', '')
        body = check['content']['body']
        description = check['description']
        fingerprint = check['fingerprint']
        begin = check['location']['lines']['begin']
        end = check['location']['lines'].get('end', None)
        path = check['location']['path']
        severity = lookup(check['severity'])
        check_type = check.get('engine_name', check['type'])

        vulnerabilities.append({
            'id': str(uuid.uuid4()),
            'name': description if len(body)>0 else name,
            'description': body if len(body)>0 else description,
            'severity': severity,
            'location': {
                'file': path,
                'start_line': begin if begin is not None else 1
            },
            'identifiers': [{
               'type': check_type,
               'name': check_type,
               'value':  check_type
            }]
        })

    if vulnerabilities:
        data_spec['vulnerabilities'] = vulnerabilities

    with open('gl-sast-report.json', 'w') as f:
        f.write(json.dumps(data_spec))

if __name__ == '__main__':
    main()
