FROM sonarsource/sonar-scanner-cli

COPY requirements.txt requirements.txt

COPY reportgen.py reportgen.py
COPY reportgencc.py reportgencc.py
COPY wait.py wait.py
COPY converter-sast.py converter-sast.py

#COPY pip-10.0.1-py2.py3-none-any.whl pip-10.0.1-py2.py3-none-any.whl.whl

#RUN python pip-10.0.1-py2.py3-none-any.whl.whl/pip install --no-index pip-10.0.1-py2.py3-none-any.whl.whl --user

RUN python3 -m ensurepip --upgrade && /tmp/.local/bin/pip3 install setuptools --user && /tmp/.local/bin/pip3 install -r requirements.txt --user

ENTRYPOINT  [""]
