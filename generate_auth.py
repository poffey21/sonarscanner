import requests

def main():
    s = requests.Session()
    r = s.get('http://192.168.7.164:8000/sessions/new?return_to=%2F')
    # 200
    payload = {'login': 'admin', 'password': 'admin'}
    url = 'http://192.168.7.164:8000/api/authentication/login'
    r = s.post(url, data=payload)
    # 200
    r = s.get('http://192.168.7.164:8000/account/reset_password')
    # 200
    url = 'http://192.168.7.164:8000/api/users/change_password'
    payload = {'login': 'admin', 'password': 'AdminAdmin123#', 'previousPassword': 'admin'}
    r = s.post(url, data=payload)

if __name__ == '__main__':
    # 100000$KC+l8P5dEcV914BOicZOhj8Svm9/s7SvYIJV7KQN5mEFZkvTNBgNl5juqEhk2g7E8tjHMbPfASYuE4csKRiy3g==
    crypted_password = '100000$KC+l8P5dEcV914BOicZOhj8Svm9/s7SvYIJV7KQN5mEFZkvTNBgNl5juqEhk2g7E8tjHMbPfASYuE4csKRiy3g=='
        # IHvCn+4N9Eh1kDdhT9tTgh5qcqM=
    salt = 'IHvCn+4N9Eh1kDdhT9tTgh5qcqM='

        # log into h2
    console = 'java -cp lib/jdbc/h2/h2-2.2.224.jar org.h2.tools.Shell -url jdbc:h2:tcp://localhost:9092/sonar'
    print(console, '\n')
    command = f"UPDATE users SET crypted_password = '{crypted_password}', salt = '{salt}', hash_method = 'PBKDF2', reset_password = False WHERE login = 'admin';"
    print(command)
        # generate token
        # sqa_90d2966f61aa9ed1a0afeec3ef1f93de350dbe48
        # hash: fe6a2b32b04052a1314b6b2050b910ce6931ae998b59511d2a50a8ab3e518481ebb69dbd261a88448ca8db830c25c07d
    token = 'sqa_90d2966f61aa9ed1a0afeec3ef1f93de350dbe48'
    token_name = 'GitLab'
    token_type = 'GLOBAL_ANALYSIS_TOKEN'
    token_hash = 'fe6a2b32b04052a1314b6b2050b910ce6931ae998b59511d2a50a8ab3e518481ebb69dbd261a88448ca8db830c25c07d'
    uuid = 'f4f68e6a-3327-45bc-a3b3-d341cb26c25f'
    bad_user_uuid = '2e549af5-4dfa-4ee8-aa94-b9ba8f1aff60' # this will be replaced
    command = f"INSERT INTO user_tokens (name, type, token_hash, uuid, user_uuid, created_at) VALUES ('{token_name}', '{token_type}', '{token_hash}', '{uuid}', '{bad_user_uuid}', 1730393498051);"
    print(command)
    # SELECT * FROM user_tokens where user_uuid = (select uuid from users where login = 'admin');
    command = f"UPDATE user_tokens SET user_uuid = (select uuid from users where login = 'admin');"
    #command = f"INSERT INTO user_tokens (name, type, token_hash, uuid, created_at) VALUES ('{token_name}', '{token_type}', '{token_hash}', '{uuid}', 1730393498051);"
    print(command)
    print(f"curl --header 'Authorization: Bearer {token}' http://localhost:8000/api/rules/search | jq")