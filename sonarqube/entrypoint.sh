#!/bin/bash
set -e

DEFAULT_CMD=('/opt/java/openjdk/bin/java' '-jar' 'lib/sonarqube.jar' '-Dsonar.log.console=true')

nohup $(echo "" > ${SONARQUBE_HOME}/logs/sonar.log && while grep -vzq "SonarQube is operational" ${SONARQUBE_HOME}/logs/sonar.log > /dev/null; do sleep 5; echo "waiting" >> /opt/sonarqube/logs/nohup.log; done && java -cp lib/jdbc/h2/h2-2.2.224.jar org.h2.tools.RunScript -url jdbc:h2:tcp://localhost:9092/sonar -script ${SONARQUBE_HOME}/update.sql  >> /opt/sonarqube/logs/nohup.log && echo "Complete") >/dev/null 2>&1 &

# this if will check if the first argument is a flag
# but only works if all arguments require a hyphenated flag
# -v; -SL; -f arg; etc will work, but not arg1 arg2
if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
    set -- "${DEFAULT_CMD[@]}" "$@"
fi

exec "$@"