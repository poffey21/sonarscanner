UPDATE users SET crypted_password = '100000$KC+l8P5dEcV914BOicZOhj8Svm9/s7SvYIJV7KQN5mEFZkvTNBgNl5juqEhk2g7E8tjHMbPfASYuE4csKRiy3g==', salt = 'IHvCn+4N9Eh1kDdhT9tTgh5qcqM=', hash_method = 'PBKDF2', reset_password = False WHERE login = 'admin';
INSERT INTO user_tokens (name, type, token_hash, uuid, user_uuid, created_at) VALUES ('GitLab', 'GLOBAL_ANALYSIS_TOKEN', 'fe6a2b32b04052a1314b6b2050b910ce6931ae998b59511d2a50a8ab3e518481ebb69dbd261a88448ca8db830c25c07d', 'f4f68e6a-3327-45bc-a3b3-d341cb26c25f', '2e549af5-4dfa-4ee8-aa94-b9ba8f1aff60', 1730393498051);
UPDATE user_tokens SET user_uuid = (select uuid from users where login = 'admin');
SELECT * FROM users;
SELECT * FROM user_tokens;