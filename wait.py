import os
import sys
import time
from pprint import pprint

import requests

from envparse import Env

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


SONAR_HOST_URL = env('SONAR_HOST_URL', default='http://localhost:9000')
SONAR_TOKEN = env('SONAR_TOKEN', default='sqa_90d2966f61aa9ed1a0afeec3ef1f93de350dbe48')


def main():
    url = '{}/api/system/status'.format(SONAR_HOST_URL)

    attempts = 0
    timed_out = False
    r = None
    MAX_WAIT = 150
    try:
        r = requests.get(url)
        while (not r.ok and attempts < 20) or (r.json().get('status', '') == 'STARTING' and attempts < MAX_WAIT):
            time.sleep(1)
            r = requests.get(url)
            if attempts > 0 and attempts % 10 == 0:
                sys.stdout.write('\n')
                sys.stdout.flush()
            sys.stdout.write('.')
            sys.stdout.flush()
            attempts += 1
    except Exception as e:
        print(e)
        exit(1)
    print('\n')

    if r is None or r.json().get('status') != 'UP':
        print('Sonarqube failed to start')
        if r:
            print(r.json().get('status'))
            print(r.status_code)
        exit(1)
    print('Sonarqube is running!')
    attempts = 0
    headers = {'Authorization': f'Bearer {SONAR_TOKEN}'}
    while attempts < MAX_WAIT:
        r = requests.get(f'{SONAR_HOST_URL}/api/rules/search', headers=headers)
        try:
            r.json()
            break
        except:
            print('Waiting since status code was still:', r.status_code)
            time.sleep(10)
            attempts += 1

if __name__ == '__main__':
    main()
